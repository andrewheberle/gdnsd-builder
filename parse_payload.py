#!/usr/bin/env python
# Quick and dirty python script to output JSON key from hook_payload file

import sys
import json

file_name = 'hook_payload'
key = sys.argv[1]

with open(file_name, 'r') as f:
    data = json.load(f)
    print(data[key])
