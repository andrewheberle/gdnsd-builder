%define _disable_ld_no_undefined 1

Name: gdnsd
Version: {{{ tagged_version }}}
Release: 1%{?dist}
Source0: https://github.com/gdnsd/gdnsd/releases/download/v%{version}/gdnsd-%{version}.tar.xz
Summary: Authoritative-only DNS server with failover support
URL: http://gdnsd.org/
License: GPLv3+
Group: System/Servers

Requires: libev
Requires: userspace-rcu
Requires: libmaxminddb >= 1.2.0
Requires: libunwind
Requires: libsodium

Requires(pre):    shadow-utils
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

BuildRequires: automake
BuildRequires: libev-devel
BuildRequires: userspace-rcu-devel
BuildRequires: libmaxminddb-devel >= 1.2.0
BuildRequires: libunwind-devel
BuildRequires: libsodium-devel
BuildRequires: systemd-devel

%description
gdnsd is an Authoritative-only DNS server. The initial g stands for Geographic,
as gdnsd offers a plugin system for geographic (or other sorts of) balancing,
redirection, and service-state-conscious failover. The plugin system can also
do things like weighted address/cname records. If you don't care about these
features you can ignore them :).

gdnsd is written in C, and uses pthreads with libev and liburcu to attain very
high performance, low latency service. It does not offer any form of caching
or recursive service, and does not support DNSSEC. There's a strong focus on
making the code efficient, lean, and resilient. The code has a decent
regression testsuite with full branch coverage on the core packet parsing and
generation code, and some scripted QA tools for e.g. valgrind validation,
clang-analyzer, etc.

The geographically-aware features also support the EDNS Client Subnet spec from
RFC 7871 for receiving more-precise network location information from
intermediate shared caches.

%prep
%setup -q
aclocal
automake -a
%configure --with-rundir=%{_rundir} --with-systemdsystemunitdir=%{_unitdir}

%build
%{__make}

%install
%{__make} install DESTDIR=%{buildroot}

# Not sure we want a -devel package here...
# Seems fairly pointless if we don't package
# any external plugins
%{__rm} -rf %{buildroot}%{_includedir} %{buildroot}%{_mandir}/man3

%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/gdnsd/zones
%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/gdnsd

%pre
getent group gdnsd >/dev/null || groupadd -r gdnsd
getent passwd gdnsd >/dev/null || \
    useradd -r -g gdnsd -d %{_sharedstatedir}/gdnsd -s /sbin/nologin \
    -c "gdnsd service account" gdnsd
exit 0

%post
%systemd_post gdnsd.service

%preun
%systemd_preun gdnsd.service

%postun
%systemd_postun_with_restart gdnsd.service

%files
%{_bindir}/gdnsd_geoip_test
%{_bindir}/gdnsdctl
%{_sbindir}/gdnsd
%{_libexecdir}/gdnsd/gdnsd_extmon_helper
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%doc %{_docdir}/gdnsd
%{_unitdir}/gdnsd.service
%{_sysconfdir}/gdnsd
%{_sharedstatedir}/gdnsd
