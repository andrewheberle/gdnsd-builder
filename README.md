# GDNSD Builder

This repository is used by [COPR](https://copr.fedorainfracloud.org/) to build RPM's for CentOS 7 and CentOS 8.

Most of this repo is historical dead-ends however the "gdnsd.spec" file is used by the build process on COPR.

## Usage

Add/Enable the copr in CentOS 8:

```sh
dnf copr enable andrewheberle/gdnsd3
```

Or CentOS 7:

```sh
yum install yum-plugin-copr
yum copr enable andrewheberle/gdnsd3
```
