#!/bin/sh
set -e
OUTDIR=$1

# Some pre-checks
[ -f "${PWD}/parse_payload.py" ] || ( echo "missing parse_payload.py" && exit 1 )
[ -d "${OUTDIR}" ] || ( echo "missing outdir" && exit 1 )
[ -f "${PWD}/hook_payload" ] || ( echo "missing hook_payload" && exit 1 )

if [ "$(python "${PWD}/parse_payload.py" object_kind)" = "tag_push" ]; then
    TAG="$(python "${PWD}/parse_payload.py" ref | cut -d'/' -f3)"
    VERSION="$(echo "${TAG}" | sed 's/v//')"
    URL="https://github.com/gdnsd/gdnsd/releases/download/${TAG}/gdnsd-${VERSION}.tar.xz"
    curl -L "${URL}" -O
    rpkg spec
    rpkg srpm --outdir "${OUTDIR}"
fi
